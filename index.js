const http = require('http');
const net = require('net');
const url = require('url');
const connectDebug = require('debug')('http-connect');

const replacements = require('./handlers').replacements;
const proxySettings = require('./proxy-settings');

const port = 9292;

// Replacement:
// - matchRequest
// - preHandler?
// - prepareOptions?
// - postHandler?

class Proxy {
  start (port) {
    http.createServer()
      .on('request', this._requestHandler.bind(this))
      .on('connect', this._connectHandler.bind(this))
      .on('error', this._commonErrorHandler)
      .listen(port);
  }

  _requestHandler (proxyRequest, proxyResponse) {
    proxyRequest.on('error', this._commonErrorHandler);
    proxyResponse.on('error', this._commonErrorHandler);

    const parsedUrl = url.parse(proxyRequest.url);

    const replacement = replacements.find(item => {
      return item.matchRequest(proxyRequest);
    });

    if (replacement && typeof replacement.preHandler === 'function') {
      if (!replacement.preHandler(proxyRequest, proxyResponse))
        return;
    }

    const options = {
      port: parsedUrl.port,
      hostname: parsedUrl.hostname,
      method: proxyRequest.method,
      path: parsedUrl.path,
      headers: proxyRequest.headers
    };

    if (replacement && typeof replacement.prepareOptions === 'function') {
      replacement.prepareOptions(options);
    }

    const realRequest = http.request(options, (realResponse) => {
      realResponse.on('error', this._commonErrorHandler);

      if (replacement && typeof replacement.postHandler === 'function') {
        if (!replacement.postHandler(proxyRequest, proxyResponse, realRequest, realResponse))
          return;
      }

      proxyResponse.writeHead(realResponse.statusCode, realResponse.headers);
      realResponse.pipe(proxyResponse);
    });

    realRequest.on('error', this._commonErrorHandler);
    proxyRequest.pipe(realRequest);
  }

  _connectHandler (proxyRequest, proxySocket, head) {
    connectDebug(proxyRequest.url);

    proxyRequest.on('error', this._commonErrorHandler);
    proxySocket.on('error', this._commonErrorHandler);

    const parsedUrl = url.parse(`http://${proxyRequest.url}`);
    const realSocket = net.connect(parsedUrl.port, parsedUrl.hostname, () => {
      proxySocket.write([
        `HTTP/${proxyRequest.httpVersion} 200 Connection Established\r\n` +
        `Proxy-agent: Node.js-Proxy\r\n` +
        `\r\n`
      ].join(''));

      realSocket.write(head);
      realSocket.pipe(proxySocket);
      proxySocket.pipe(realSocket);
    });

    realSocket.on('error', this._commonErrorHandler);
  }

  _commonErrorHandler (error) {
    console.error(error.message);
  }
}

// init ---

new Proxy().start(port);
proxySettings('127.0.0.1:' + port);

// exit ---

process.stdin.resume();
process.on('exit', exitHandler.bind(null, { cleanup: true }));
process.on('SIGINT', exitHandler.bind(null, { exit: true }));
process.on('uncaughtException', exitHandler.bind(null, { exit: true }));

function exitHandler (options, err) {
  if (options.cleanup)
    proxySettings(false);
  if (err)
    console.error(err.stack);
  if (options.exit)
    process.exit();
}
