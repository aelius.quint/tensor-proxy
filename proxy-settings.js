'use strict';

const spawn = require('child_process').spawnSync;
const debug = require('debug')('proxy-settings');

const node = 'HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings';

// http://superuser.com/questions/419696/in-windows-7-how-to-change-proxy-settings-from-command-line
// Google: DefaultConnectionSettings

module.exports = function (server) {
  debug(server);

  const enabled = !!server;
  if (enabled)
    server = 'http=' + server;

  setRegValue(node, 'ProxyEnable', 'REG_DWORD', enabled | 0);
  if (enabled)
    setRegValue(node, 'ProxyServer', 'REG_SZ', server);

  const settings = buildConnectionSettings(enabled, enabled ? server : '');
  setRegValue(node + '\\Connections', 'DefaultConnectionSettings', 'REG_BINARY', settings.toString('hex'));
};

function setRegValue (node, key, type, value) {
  spawn('reg', ['add', node, '/v', key, '/t', type, '/d', value, '/f']);
}

function buildConnectionSettings (enabled, server) {
  const LOOPBACK = '<-loopback>';

  const b1 = new Buffer(16);
  const b2 = new Buffer(4);
  const b3 = new Buffer(4);
  const b4 = new Buffer(32);
  const b5 = new Buffer(8 * 16); // unknown

  // 1) Byte number zero always has a 3C or 46 - I couldn't find more
  // information about this byte. The next three bytes are zeros.
  b1.writeInt32LE(0x46, 0);

  // 2) Byte number 4 is a counter used by the 'Internet Options' property
  // sheet (Internet explorer->Tools->Internet Options...). As you manually
  // change the internet setting (such as LAN settings in the Connections tab),
  // this counter increments.Its not very useful byte.But it MUST have a value.
  // I keep it zero always.The next three bytes are zeros (Bytes 5 to 7).
  b1.writeInt32LE(Math.random() * (1024 - 1) + 1, 4);

  // 3) Byte number 8 can take different values as per your settings. The value is:
  // - 09 when only 'Automatically detect settings' is enabled
  // - 03 when only 'Use a proxy server for your LAN' is enabled [***]
  // - 0B when both are enabled
  // - 05 when only 'Use automatic configuration script' is enabled
  // - 0D when 'Automatically detect settings' and 'Use automatic configuration script' are enabled
  // - 07 when 'Use a proxy server for your LAN' and 'Use automatic configuration script' are enabled
  // - 0F when all the three are enabled
  // - 01 when none of them are enabled [***]
  // - The next three bytes are zeros (Bytes 9 to B).
  b1.writeInt32LE(enabled ? 3 : 1, 8);

  // 4) Byte number C (12 in decimal) contains the length of the proxy server
  // address. For example a proxy server '127.0.0.1:80' has length 12 (length
  // includes the dots and the colon). The next three bytes are zeros (Bytes D to F).
  b1.writeInt32LE(server.length, 12);

  // 5) Byte 10 (or 16 in decimal) contains the proxy server address - like
  // '127.0.0.1:80' (where 80 is obviously the port number)
  /* nothing to do */

  // 6) the byte immediately after the address contains the length of additional
  // information. The next three bytes are zeros. For example if the 'Bypass
  // proxy server for local addresses' is ticked, then this byte is 07, the next
  // three bytes are zeros and then comes a string i.e. '' (indicates that you
  // are bypassing the proxy server. Now since has 7 characters, the length is
  // 07!). You will have to experiment on your own for finding more about this.
  // If you dont have any additional info then the length is 0 and no
  // information is added.
  b2.writeInt32LE(LOOPBACK.length, 0);

  // 7) The byte immediately after the additional info, is the length of the
  // automatic configuration script address (If you don't have a script address
  // then you don't need to add anything, skip this step and go to step 8).
  // The next three bytes are zeros,then comes the address.
  b3.writeInt32LE(0, 0);

  // 8) Finally, 32 zeros are appended.(I dont know why!)
  b4.fill(0);

  // Unknown data
  b5.fill(0);
  b5.writeInt8(0x02, 0);
  b5.writeInt8(0x0A, 4);
  b5.writeInt8(0x10, 5);
  b5.writeInt8(0x02, 6);
  b5.writeInt8(0x73, 7);

  return Buffer.concat([
    b1,
    new Buffer(server),
    b2,
    new Buffer(LOOPBACK),
    b3,
    b4,
    b5
  ]);
}
