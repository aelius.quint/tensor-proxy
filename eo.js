'use strict';

const assert = require('assert');

const fs = require('fs');
const url = require('url');
const cookie = require('cookie');
const spawn = require('child_process').spawnSync;

const repoWatcherDebug = require('debug')('repo-watcher');
const fileReplacementDebug = require('debug')('file-replacement');
const contentsReplacementDebug = require('debug')('contents-replacement');
const fastStaticReplacementDebug = require('debug')('fast-static-replacement');
const rpcReplacementDebug = require('debug')('rpc-replacement');

const DEFAULT_REPO = 'eo';
const REPOS_ROOT = 'C:\\Repos';

//
// File representation
//

class File {
  constructor () {
    if (arguments.length === 2) { // 'repo', 'local/file'
      this._repo = arguments[0];
      this._relative = relative(arguments[1]);

    } else if (arguments.length === 1) {
      const path = arguments[0];
      const index = path.indexOf(REPOS_ROOT);

      if (index === 0) { // 'C:\\Repos\\hello\\local\\file'
        const match = path.substr(REPOS_ROOT.length).match(/^\\([^\\]+)\\(.*)$/);
        if (!match)
          throw new Error('Bad File arguments: ' + Array.from(arguments).join(', '));

        this._repo = match[1];
        this._relative = relative(match[2]);

      } else if (/^[A-Z]:/.test(arguments[0])) { // 'C:\\other\\file'
        this._absolute = absolute(path);

      } else { // 'local/file'
        this._repo = DEFAULT_REPO;
        this._relative = relative(arguments[0]);
      }
    }

    if (!this._absolute)
      this._absolute = absolute(getRepoDir(this._repo) + '\\' + this._relative);

    function absolute (string) {
      return string.replace(/\//g, '\\');
    }

    function relative (string) {
      return string.replace(/\\/g, '/');
    }
  }

  getAbsolutePath () {
    return this._absolute;
  }

  getDeployUrl () {
    if (!this._relative)
      throw new Error('File ' + this._absolute + ' does not contain relative path');
    return translit(this._relative.replace(/^client\//, '/')).replace(/\s/g, '_');
  }

  getModuleName () {
    if (this._moduleName === undefined) {
      const data = fs.readFileSync(this._absolute).toString();
      const match = data.match(/^\s*define\s*\(['"]js!([^'"]+)['"]/);
      this._moduleName = match ? match[1] : null;
    }
    return this._moduleName;
  }

  createReader () {
    return fs.createReadStream(this._absolute);
  }
}

//
// Replacement base
//

class Replacement {
  matchRequest (proxyRequest) {
    return false;
  }

  preHandler (proxyRequest, proxyResponse) {
    return true;
  }

  prepareOptions (realRequestOptions) {
    // nothing to do
  }

  postHandler (proxyRequest, proxyResponse, realRequest, realResponse) {
    return true;
  }
}

//
// File replacer
//

class FileReplacement extends Replacement {
  constructor (file) {
    super();
    this._file = file;

    // TODO use deploy url
    const absolute = file.getAbsolutePath();
    const index = absolute.indexOf('\\components\\');
    if (index === -1)
      throw new Error('/components/ is required');

    const componentPath = absolute.substr(index + '\\components\\'.length);

    const regex = new RegExp(
      '^.*\\/' +
      componentPath
        .replace(/\\/g, '\\/')
        .replace(/\.module\.js$/, '.module.(v[0-9a-f]{45}.)?js')
        .replace(/\./, '\\.') +
      '$'
    );

    fileReplacementDebug(regex);
    this._regex = regex;
  }

  matchRequest (proxyRequest) {
    return this._regex.test(proxyRequest.url);
  }

  preHandler (proxyRequest, proxyResponse) {
    const stream = this._file.createReader();
    stream.on('error', error => console.error(error.message));
    stream.pipe(proxyResponse);
    return false;
  }
}

//
// contents.js modifier
//

class ContentsReplacement extends Replacement {
  constructor (files) {
    super();

    this._patch = { js: [] };
    for (let file of files) {
      if (!/\.js$/.test(file.getAbsolutePath()))
        continue;

      const module = file.getModuleName();
      if (!module)
        continue;

      const deployUrl = file.getDeployUrl();
      this._patch.js[module] = file.getDeployUrl();

      contentsReplacementDebug('%s => %s', module, deployUrl);
    }
  }

  matchRequest (proxyRequest) {
    return /\/debug\/resources\/contents\.js$/.test(proxyRequest.url);
  }

  prepareOptions (options) {
    delete options.headers['accept-encoding'];
    delete options.headers['if-none-match'];
    delete options.headers['if-modified-since'];
  }

  postHandler (proxyRequest, proxyResponse, realRequest, realResponse) {
    collect(realResponse).then(data => {
      const json = eval('(' + data.replace(/;$/, '').replace(/^contents = /, '') + ')');

      for (let module of Object.keys(this._patch.js)) {
        json['jsModules'][module] = this._patch.js[module];
      }

      proxyResponse.writeHead(200, { 'Content-Type': 'application/x-javascript' });
      proxyResponse.write('contents = ' + JSON.stringify(json, void 0, 2) + ';');
      proxyResponse.end();
    });
  }
}

//
// Get static files from faster test-online stand
//

class FastStaticReplacement extends Replacement {
  constructor (options) {
    super();

    this._remoteStandCookie = options.remoteStandCookie;
    this._localStandCookie  = options.localStandCookie;
    this._localStandParam = cookie.parse(this._localStandCookie).param;

    assert(this._localStandParam);
    fastStaticReplacementDebug('local stand param is: %s', this._localStandParam);
  }

  matchRequest (proxyRequest) {
    if (!proxyRequest.headers['cookie'])
      return false;

    const param = cookie.parse(proxyRequest.headers['cookie']).param;
    if (param !== this._localStandParam)
      return false;

    const pathname = url.parse(proxyRequest.url).pathname;

    if ([                   // грузим с локального стенда:
      /\/editPage\.html$/,  // - страницу отчета
      /\/contents\.json$/,  // - список модулей
      /^\/specifications\// // - js "спецификаций"
    ].find(regexp => regexp.test(pathname)))
      return false;

    return /\.(x?html|css|js|png|jpg|gif|woff|json|tmpl)$/.test(pathname);
  }

  prepareOptions (options) {
    delete options.headers['accept-encoding'];
    delete options.headers['if-none-match'];
    delete options.headers['if-modified-since'];
    options.headers['cookie'] = this._remoteStandCookie;
    if (!/\/(cdn|debug)/.test(options.path))
      options.path = '/debug' + options.path;
  }
}

//
// RPC mock
//

class RPCReplacement extends Replacement {
  constructor (methods) {
    super();

    this._methods = methods;

    const keys = Array.from(this._methods.keys());
    rpcReplacementDebug('defined methods: %s', keys.length ? keys.join(', ') : '(none)');
  }

  matchRequest (proxyRequest) {
    if (!/sbis-rpc-service300.dll$/.test(proxyRequest.url))
      return false;

    if (!proxyRequest.headers['x-originalmethodname'])
      return false;

    const methodName = Buffer.from(proxyRequest.headers['x-originalmethodname'], 'base64').toString();
    return this._methods.has(methodName);
  }

  preHandler (proxyRequest, proxyResponse) {
    collect(proxyRequest).then(data => {
      let json;

      try {
        json = JSON.parse(data);
      } catch (error) {
        rpcReplacementDebug('not a json', data);
        return proxyResponse.writeHead(500, 'Not a JSON');
      }

      const methodName = json.method;
      if (!this._methods.has(methodName)) {
        const originalName = Buffer.from(proxyRequest.headers['x-originalmethodname'], 'base64').toString();
        rpcReplacementDebug('method names mismatch: %s != %s', originalName, methodName);
        return proxyResponse.writeHead(500, `Method names mismatch: ${originalName} != ${methodName}`);
      }

      rpcReplacementDebug('calling: %s', methodName);

      let status;
      const response = { id: 1, jsonrpc: '2.0', protocol: 4 };
      try {
        response.result = this._methods.get(methodName)(json.params);
        status = 200;
      } catch (value) {
        response.error = value;
        status = 500;
      }

      proxyResponse.writeHead(status, { 'Content-Type': 'application/json; charset=utf-8' });
      proxyResponse.write(JSON.stringify(response));
      proxyResponse.end();
    }).then(null, err => {
      rpcReplacementDebug(err);
    });

    return false;
  }
}

function translit (string) {
  const table = {
    'а': 'a',  'б': 'b',  'в': 'v',
    'г': 'g',  'д': 'd',  'е': 'e',
    'ё': 'yo', 'ж': 'zh', 'з': 'z',
    'и': 'i',  'й': 'i',  'к': 'k',
    'л': 'l',  'м': 'm',  'н': 'n',
    'о': 'o',  'п': 'p',  'р': 'r',
    'с': 's',  'т': 't',  'у': 'u',
    'ф': 'f',  'х': 'h',  'ц': 'c',
    'ч': 'ch', 'ш': 'sh', 'щ': 'sch',
    'ъ': '',   'ы': 'y',  'ь': '',
    'э': 'e',  'ю': 'yu', 'я': 'ya',
  };

  return Array.from(string).map(char => {
    const lowerChar = char.toLowerCase();

    if (table.hasOwnProperty(lowerChar)) {
      const latinChar = table[lowerChar];

      if (char === lowerChar)
        return latinChar;

      if (latinChar.length === 1)
        return latinChar.toUpperCase();

      return latinChar.charAt(0).toUpperCase() + latinChar.substr(1);
    }

    return char;
  }).join('');
}

//
// Git Actions
//

function listChangedFiles (repo) {
  return git(repo, ['ls-files', '-m']);
}

function listAddedFiles (repo) {
  return git(repo, ['diff', '--cached', '--name-only', '--diff-filter=A']);
}

function git (repo, args) {
  repo = repo || DEFAULT_REPO;
  const options = { cwd: getRepoDir(repo) };
  const output = spawn('git', ['-c', 'core.quotepath=false'].concat(args), options).stdout;
  return output.toString().split('\n').filter(l => !!l).map(l => new File(repo, l));
}

function getRepoDir (repo) {
  return REPOS_ROOT + '\\' + repo;
}

function watchRepo (repo, callback) {
  if (typeof repo === 'function') {
    callback = repo;
    repo = undefined;
  }
  repo = repo || DEFAULT_REPO;

  let timeout = null;
  const invoke = () => {
    if (timeout)
      clearTimeout(timeout);
    timeout = setTimeout(() => {
      timeout = null;
      repoWatcherDebug('changes detected in repo %s', repo);
      callback();
    }, 500);
  };

  const repoDir = getRepoDir(repo);
  fs.watch(repoDir, { recursive: true }, (eventType, fileName) => {
    if (/^\.(git|idea)/.test(fileName))
      return false;

    if (/___jb_...___$/.test(fileName))
      return false;

    invoke();
  });
}

function collect (readable) {
  let buffer = new Buffer([]);

  return new Promise((resolve, reject) => {
    readable
      .on('error', reject)
      .on('data', chunk => buffer = Buffer.concat([buffer, chunk]))
      .on('end', () => {
        resolve(buffer.toString());
      });
  });
}

module.exports = {
  listChangedFiles,
  listAddedFiles,
  watchRepo,
  File,
  FileReplacement,
  ContentsReplacement,
  FastStaticReplacement,
  RPCReplacement
};
