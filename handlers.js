'use strict';

const listChangedFiles = require('./eo').listChangedFiles;
const listAddedFiles = require('./eo').listAddedFiles;
const watchRepo = require('./eo').watchRepo;
const FileReplacement = require('./eo').FileReplacement;
const ContentsReplacement = require('./eo').ContentsReplacement;
const RPCReplacement = require('./eo').RPCReplacement;
const FastStaticReplacement = require('./eo').FastStaticReplacement;
const File = require('./eo').File;

const methods = new Map([
  ['Комплект.IsFillClosedReports', () => {
    return false;
  }]
]);

// Обычный --- v
const replacements = {
  items: [],

  find () {
    return this.items.find.apply(this.items, arguments);
  },

  reload () {
    const allModifiedFiles = [
      ...listAddedFiles(),
      ...listChangedFiles()
    ];

    this.items = [
      new RPCReplacement(methods),
      ...allModifiedFiles.map(file => new FileReplacement(file))
    ];

    if (allModifiedFiles.length)
      this.items.push(new ContentsReplacement(allModifiedFiles));
  }
};

watchRepo(() => {
  replacements.reload()
});

replacements.reload();
// Обычный --- ^

// Локальный стенд --- v
// const replacements = [
//   ...[
//     ...listAddedFiles(),
//     ...listChangedFiles()
//   ].map(file => new FileReplacement(file)),
//
//   new FastStaticReplacement({
//     // remoteStandCookie: 's3ps=23d4d910-9892-4599-82b3-1279596287ed-000bf295; hostname=r16-comp16; tz=-180; s3ai=1475160761748%20%3B%20false%20%3B%205675271; _ym_uid=1467303167182828951; su=000bf295-00564df1; _ga=GA1.2.252183057.1465294105; leftAccCompact=; param=%25u0413%25u0440%25u0443%25u0431%25u0435%25u0440; sid=000bf295-00569907-00ba-16491dbd557f4e47; s3su=000bf295-00569907; CpsUserId=23d4d910-9892-4599-82b3-1279596287ed; did=b80eb67a-0f26-8cbe-14b1-b0b507405abc; s3ttc=Thu%2C%2029%20Sep%202016%2008%3A56%3A15%20GMT; s3rh=9a406b079322d796580f529103c7af75; _ym_isad=1',
//     remoteStandCookie: 'hostname=r16-comp16; s3ai=1475166226946%20%3B%20false%20%3B%205675271; s3ps=18267163-65ef-4307-9b83-a6885016b796-005601a7; tz=-180; _ym_uid=1467303167182828951; su=000bf295-00564df1; _ga=GA1.2.252183057.1465294105; _ym_isad=1; leftAccCompact=; param=%25u043D%25u043E%25u0432%25u044B%25u0439%25u0441%25u0432%25u0435%25u0442; sid=005601a7-005601a8-00ba-816290ddd77242b3; s3su=005601a7-005601a8; CpsUserId=18267163-65ef-4307-9b83-a6885016b796; did=b80eb67a-0f26-8cbe-14b1-b0b507405abc; s3ttc=Thu%2C%2029%20Sep%202016%2016%3A23%3A59%20GMT; s3rh=029ca95b1916512a85f4d4c30ab23b02; s3ps=18267163-65ef-4307-9b83-a6885016b796-005601a7; hostname=r16-comp16; s3ai=1475166244795%20%3B%20true%20%3B%205636520; tz=-180',
//     localStandCookie: 'leftAccCompact=; param=yunusovnew; sid=005601a7-00566680-00ba-20231440736f4951; s3su=005601a7-00566680; CpsUserId=7d83d473-e81a-4b5d-a018-b4c260d066d6; did=b80eb67a-0f26-8cbe-14b1-b0b507405abc; s3ttc=Thu%2C%2029%20Sep%202016%2014%3A23%3A26%20GMT; s3rh=9573523c65080e5623f2545814421aaa; hostname=r16-comp16; tz=-180'
//   })
// ];
// Локальный стенд --- ^

module.exports.methods = methods;
module.exports.replacements = replacements;

function error (message, details) {
  return {
    code: -32000,
    message: message,
    details: details || message,
    type: "error",
    data: {
      classid: "{afd28339-dc44-4ad9-96dc-55a9789c743a}",
      error_code: -1,
      addinfo: null
    }
  };
}
